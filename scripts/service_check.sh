#!/bin/bash

# Title: Service Check Script
# Author: Tyler Higgins
# Date: 10-2-2020

function checkIt()
{
	if systemctl is-active --quiet $1;then
		echo "$1 is active."
	else
		echo "$1 is NOT active. Trying to start $1 now."
		systemctl start $1
		if systemctl is-active --quiet $1;then
			echo "$1 was started successfully."
		else
			echo "$1 could not be started."
		fi
}

checkIt "nginx"
checkIt "mysql"
checkIt "php5-fpm"