#!/bin/bash

# Author: Tyler Higgins
# Date: 10-03-2020
# Simple setup script to get server setup for WRCCDC

# Get the OS up-to-date
sudo apt update && sudo apt upgrade -y && sudo apt dist-upgrade -y

# Check for ufw service, if it does not exist install it.    
if service --status-all | grep -Fq 'ufw'; then    
	echo "ufw firewall is installed."

else
	echo "ufw firewall is not installed, trying to install it now."
	sudo apt install ufw -y 

fi

sudo ufw default deny incoming
sudo ufw default allow outgoing
sudo ufw allow ssh
sudo ufw allow http
sudo ufw allow https
sudo ufw --force enable
sudo ufw logging medium
